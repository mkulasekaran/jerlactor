Jinterface Extended to support actor based java processing.

**How is it different from jinterface?**
jerlactor is wrapper around jinterface but will allow you to use reactive pattern on the java end for processing instead of the threaded pattern for processing.

**Does it do any good?**
For tasks which require no thread switch and for computation heavy tasks for example erlang does message passing and you need to do some serious math on the java end.

**Internally how jinterface works?**
There is a blocking queue on each of the threads and once the main thread(thread which connects to epmd) receives a message it notify the thread queue.

Steps to follow:

start the erlang node
**erl -sname user --cookie cookie**.
TestActor in src/test folder,
in the below example i used 8 executor threads which is like using 8 eventloops.
Note: eventloop in java are not kernel threads unlike erlang schedulars.


CODE:
```
public class JerlactorTest {
	
public void connectNode() throws InterruptedException {
		int THREADSIZE = 100000;
		OtpNode node = null;
		ExecutorService pool = Executors.newWorkStealingPool(8);
		try {
			node = new OtpNode("user@localhost", "cookie"); 
		} catch (IOException ex) {
			System.exit(-1);
		}
		System.out.println("Connected to epmd...");

		if (node.ping("shell@localhost", 2000)) {
			System.out.println("shell@localhost is up.");
		} else {
			System.out.println("shell@localhost is down");
		}
		PoolFiberFactory pff = new PoolFiberFactory(pool);
		for (int i = 0; i < THREADSIZE; i++) {
			new TestActor(node, "mbox" + i, pff);
		}
		Thread.sleep(Integer.MAX_VALUE);

	}

	public static void main(String[] args) throws InterruptedException {
	new JerlactorTest().connectNode();
	}}
```