package com.jerlactor.test;

import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.jetlang.fibers.PoolFiberFactory;

import com.ericsson.otp.erlang.OtpNode;

//import com.jerlactor.actor.ActorMailBox;

public class TryOTP {

	// erl -sname user@localhost -setcookie cookie
	// net_adm:ping(javambox@localhost)
	// {mbox, javambox@localhost} ! hello.

	public void connectingmultipleNode() throws InterruptedException {
		OtpNode node = null;
		ExecutorService pool = Executors.newWorkStealingPool(8);
		try {
			node = new OtpNode("javambox@localhost", "cookie"); // name,
																// cookie
		} catch (IOException ex) {
			System.exit(-1);
		}
		System.out.println("Connected to epmd...");

		if (node.ping("user@localhost", 2000)) {
			System.out.println("user@localhost is up.");
		} else {
			System.out.println("user@localhost is down");
		}
		PoolFiberFactory pff = new PoolFiberFactory(pool);
		// for (int i = 0; i < THREADSIZE; i++) {
		new TestActor(node, "mbox", pff);
		new TestActor(node, "mbox2", pff);
		// }
		Thread.sleep(Integer.MAX_VALUE);

	}

	public static void main(String[] args) throws InterruptedException {
		// System.getProperties().setProperty("OtpConnection.trace", "3");
		new TryOTP().connectingmultipleNode();
	}

}