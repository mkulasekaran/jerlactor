package com.jerlactor.test;

import org.jetlang.fibers.PoolFiberFactory;

import com.ericsson.otp.erlang.OtpErlangDecodeException;
import com.ericsson.otp.erlang.OtpMsg;
import com.ericsson.otp.erlang.OtpNode;
import com.jerlactor.actor.TaskActor;

public class TestActor extends TaskActor {

	public TestActor(OtpNode node, String name, PoolFiberFactory factory) {
		super(node, name, factory);
	}

	public void onMessage(OtpMsg message) {
		try {
			System.out.println(message.getSenderPid());
			send(message.getSenderPid(), message.getMsg());
		} catch (OtpErlangDecodeException e) {
			e.printStackTrace();
		}
	}

}
