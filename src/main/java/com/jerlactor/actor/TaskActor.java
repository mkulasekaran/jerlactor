package com.jerlactor.actor;

import org.jetlang.channels.Channel;
import org.jetlang.channels.MemoryChannel;
import org.jetlang.fibers.Fiber;
import org.jetlang.fibers.PoolFiberFactory;

import com.ericsson.otp.erlang.OtpErlangObject;
import com.ericsson.otp.erlang.OtpErlangPid;
import com.ericsson.otp.erlang.OtpMsg;
import com.ericsson.otp.erlang.OtpNode;

public abstract class TaskActor implements ActorPublisher {

	Channel<OtpMsg> outChannel;
	ActorMailBox actormailbox;

	public TaskActor(OtpNode node, String name, PoolFiberFactory factory) {
		actormailbox = node.createMbox(name, this);
		outChannel = new MemoryChannel<OtpMsg>();
		Fiber fiber = factory.create();
		outChannel.subscribe(fiber, this);
		fiber.start();
	}

	public ActorMailBox getmbox() {
		return actormailbox;
	}

	public void deliver(OtpMsg msg) {
		outChannel.publish(msg);
	}

	public void send(OtpErlangPid to, OtpErlangObject msg) {
		actormailbox.send(to, msg);
	}

	public void send(String aname, OtpErlangObject msg) {
		actormailbox.send(aname, msg);
	}

	public void send(String aname, String node, OtpErlangObject msg) {
		actormailbox.send(aname, node, msg);
	}

	public void close() {
		actormailbox.close();
	}

}
