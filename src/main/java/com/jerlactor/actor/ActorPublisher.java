package com.jerlactor.actor;

import org.jetlang.core.Callback;

import com.ericsson.otp.erlang.OtpMsg;

public interface ActorPublisher extends Callback<OtpMsg> {
	public void deliver(OtpMsg msg);
}
