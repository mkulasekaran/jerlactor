package com.jerlactor.actor;

import com.ericsson.otp.erlang.OtpErlangDecodeException;
import com.ericsson.otp.erlang.OtpErlangExit;
import com.ericsson.otp.erlang.OtpErlangObject;
import com.ericsson.otp.erlang.OtpErlangPid;
import com.ericsson.otp.erlang.OtpInputStream;
import com.ericsson.otp.erlang.OtpMbox;
import com.ericsson.otp.erlang.OtpMsg;
import com.ericsson.otp.erlang.OtpNode;

public interface ActorMailBox {

	/**
	 * <p>
	 * Get the identifying {@link OtpErlangPid pid} associated with this
	 * mailbox.
	 * </p>
	 * 
	 * <p>
	 * The {@link OtpErlangPid pid} associated with this mailbox uniquely
	 * identifies the mailbox and can be used to address the mailbox. You can
	 * send the {@link OtpErlangPid pid} to a remote communicating part so that
	 * he can know where to send his response.
	 * </p>
	 * 
	 * @return the self pid for this mailbox.
	 */
	public abstract OtpErlangPid self();

	/**
	 * <p>
	 * Register or remove a name for this mailbox. Registering a name for a
	 * mailbox enables others to send messages without knowing the
	 * {@link OtpErlangPid pid} of the mailbox. A mailbox can have at most one
	 * name; if the mailbox already had a name, calling this method will
	 * supercede that name.
	 * </p>
	 * 
	 * @param aname
	 *            the name to register for the mailbox. Specify null to
	 *            unregister the existing name from this mailbox.
	 * 
	 * @return true if the name was available, or false otherwise.
	 */
	public abstract boolean registerName(String aname);

	/**
	 * Get the registered name of this mailbox.
	 * 
	 * @return the registered name of this mailbox, or null if the mailbox had
	 *         no registered name.
	 */
	public abstract String getName();

	/**
	 * Block until a message arrives for this mailbox.
	 * 
	 * @return an {@link OtpErlangObject OtpErlangObject} representing the body
	 *         of the next message waiting in this mailbox.
	 * 
	 * @exception OtpErlangDecodeException
	 *                if the message can not be decoded.
	 * 
	 * @exception OtpErlangExit
	 *                if a linked {@link OtpErlangPid pid} has exited or has
	 *                sent an exit signal to this mailbox.
	 */
	public abstract OtpErlangObject receive() throws OtpErlangExit,
			OtpErlangDecodeException;

	/**
	 * Wait for a message to arrive for this mailbox.
	 * 
	 * @param timeout
	 *            the time, in milliseconds, to wait for a message before
	 *            returning null.
	 * 
	 * @return an {@link OtpErlangObject OtpErlangObject} representing the body
	 *         of the next message waiting in this mailbox.
	 * 
	 * @exception OtpErlangDecodeException
	 *                if the message can not be decoded.
	 * 
	 * @exception OtpErlangExit
	 *                if a linked {@link OtpErlangPid pid} has exited or has
	 *                sent an exit signal to this mailbox.
	 */
	public abstract OtpErlangObject receive(long timeout) throws OtpErlangExit,
			OtpErlangDecodeException;

	/**
	 * Block until a message arrives for this mailbox.
	 * 
	 * @return a byte array representing the still-encoded body of the next
	 *         message waiting in this mailbox.
	 * 
	 * @exception OtpErlangExit
	 *                if a linked {@link OtpErlangPid pid} has exited or has
	 *                sent an exit signal to this mailbox.
	 * 
	 */
	public abstract OtpInputStream receiveBuf() throws OtpErlangExit;

	/**
	 * Wait for a message to arrive for this mailbox.
	 * 
	 * @param timeout
	 *            the time, in milliseconds, to wait for a message before
	 *            returning null.
	 * 
	 * @return a byte array representing the still-encoded body of the next
	 *         message waiting in this mailbox.
	 * 
	 * @exception OtpErlangExit
	 *                if a linked {@link OtpErlangPid pid} has exited or has
	 *                sent an exit signal to this mailbox.
	 * 
	 * @exception InterruptedException
	 *                if no message if the method times out before a message
	 *                becomes available.
	 */
	public abstract OtpInputStream receiveBuf(long timeout)
			throws InterruptedException, OtpErlangExit;

	/**
	 * Block until a message arrives for this mailbox.
	 * 
	 * @return an {@link OtpMsg OtpMsg} containing the header information as
	 *         well as the body of the next message waiting in this mailbox.
	 * 
	 * @exception OtpErlangExit
	 *                if a linked {@link OtpErlangPid pid} has exited or has
	 *                sent an exit signal to this mailbox.
	 * 
	 */
	public abstract OtpMsg receiveMsg() throws OtpErlangExit;

	/**
	 * Wait for a message to arrive for this mailbox.
	 * 
	 * @param timeout
	 *            the time, in milliseconds, to wait for a message.
	 * 
	 * @return an {@link OtpMsg OtpMsg} containing the header information as
	 *         well as the body of the next message waiting in this mailbox.
	 * 
	 * @exception OtpErlangExit
	 *                if a linked {@link OtpErlangPid pid} has exited or has
	 *                sent an exit signal to this mailbox.
	 * 
	 * @exception InterruptedException
	 *                if no message if the method times out before a message
	 *                becomes available.
	 */
	public abstract OtpMsg receiveMsg(long timeout)
			throws InterruptedException, OtpErlangExit;

	/**
	 * Send a message to a remote {@link OtpErlangPid pid}, representing either
	 * another {@link OtpMbox mailbox} or an Erlang process.
	 * 
	 * @param to
	 *            the {@link OtpErlangPid pid} identifying the intended
	 *            recipient of the message.
	 * 
	 * @param msg
	 *            the body of the message to send.
	 * 
	 */
	public abstract void send(OtpErlangPid to, OtpErlangObject msg);

	/**
	 * Send a message to a named mailbox created from the same node as this
	 * mailbox.
	 * 
	 * @param aname
	 *            the registered name of recipient mailbox.
	 * 
	 * @param msg
	 *            the body of the message to send.
	 * 
	 */
	public abstract void send(String aname, OtpErlangObject msg);

	/**
	 * Send a message to a named mailbox created from another node.
	 * 
	 * @param aname
	 *            the registered name of recipient mailbox.
	 * 
	 * @param node
	 *            the name of the remote node where the recipient mailbox is
	 *            registered.
	 * 
	 * @param msg
	 *            the body of the message to send.
	 * 
	 */
	public abstract void send(String aname, String node, OtpErlangObject msg);

	/**
	 * Close this mailbox with the given reason.
	 * 
	 * <p>
	 * After this operation, the mailbox will no longer be able to receive
	 * messages. Any delivered but as yet unretrieved messages can still be
	 * retrieved however.
	 * </p>
	 * 
	 * <p>
	 * If there are links from this mailbox to other {@link OtpErlangPid pids},
	 * they will be broken when this method is called and exit signals will be
	 * sent.
	 * </p>
	 * 
	 * @param reason
	 *            an Erlang term describing the reason for the exit.
	 */
	public abstract void exit(OtpErlangObject reason);

	/**
	 * Equivalent to <code>exit(new OtpErlangAtom(reason))</code>.
	 * 
	 * @see #exit(OtpErlangObject)
	 */
	public abstract void exit(String reason);

	/**
	 * <p>
	 * Send an exit signal to a remote {@link OtpErlangPid pid}. This method
	 * does not cause any links to be broken, except indirectly if the remote
	 * {@link OtpErlangPid pid} exits as a result of this exit signal.
	 * </p>
	 * 
	 * @param to
	 *            the {@link OtpErlangPid pid} to which the exit signal should
	 *            be sent.
	 * 
	 * @param reason
	 *            an Erlang term indicating the reason for the exit.
	 */
	// it's called exit, but it sends exit2
	public abstract void exit(OtpErlangPid to, OtpErlangObject reason);

	/**
	 * <p>
	 * Equivalent to <code>exit(to, new
	 * OtpErlangAtom(reason))</code>.
	 * </p>
	 * 
	 * @see #exit(OtpErlangPid, OtpErlangObject)
	 */
	public abstract void exit(OtpErlangPid to, String reason);

	/**
	 * <p>
	 * Link to a remote mailbox or Erlang process. Links are idempotent, calling
	 * this method multiple times will not result in more than one link being
	 * created.
	 * </p>
	 * 
	 * <p>
	 * If the remote process subsequently exits or the mailbox is closed, a
	 * subsequent attempt to retrieve a message through this mailbox will cause
	 * an {@link OtpErlangExit OtpErlangExit} exception to be raised. Similarly,
	 * if the sending mailbox is closed, the linked mailbox or process will
	 * receive an exit signal.
	 * </p>
	 * 
	 * <p>
	 * If the remote process cannot be reached in order to set the link, the
	 * exception is raised immediately.
	 * </p>
	 * 
	 * @param to
	 *            the {@link OtpErlangPid pid} representing the object to link
	 *            to.
	 * 
	 * @exception OtpErlangExit
	 *                if the {@link OtpErlangPid pid} referred to does not exist
	 *                or could not be reached.
	 * 
	 */
	public abstract void link(OtpErlangPid to) throws OtpErlangExit;

	/**
	 * <p>
	 * Remove a link to a remote mailbox or Erlang process. This method removes
	 * a link created with {@link #link link()}. Links are idempotent; calling
	 * this method once will remove all links between this mailbox and the
	 * remote {@link OtpErlangPid pid}.
	 * </p>
	 * 
	 * @param to
	 *            the {@link OtpErlangPid pid} representing the object to unlink
	 *            from.
	 * 
	 */
	public abstract void unlink(OtpErlangPid to);

	/**
	 * <p>
	 * Create a connection to a remote node.
	 * </p>
	 * 
	 * <p>
	 * Strictly speaking, this method is not necessary simply to set up a
	 * connection, since connections are created automatically first time a
	 * message is sent to a {@link OtpErlangPid pid} on the remote node.
	 * </p>
	 * 
	 * <p>
	 * This method makes it possible to wait for a node to come up, however, or
	 * check that a node is still alive.
	 * </p>
	 * 
	 * <p>
	 * This method calls a method with the same name in {@link OtpNode#ping
	 * Otpnode} but is provided here for convenience.
	 * </p>
	 * 
	 * @param node
	 *            the name of the node to ping.
	 * 
	 * @param timeout
	 *            the time, in milliseconds, before reporting failure.
	 */
	public abstract boolean ping(String node, long timeout);

	/**
	 * <p>
	 * Get a list of all known registered names on the same {@link OtpNode node}
	 * as this mailbox.
	 * </p>
	 * 
	 * <p>
	 * This method calls a method with the same name in {@link OtpNode#getNames
	 * Otpnode} but is provided here for convenience.
	 * </p>
	 * 
	 * @return an array of Strings containing all registered names on this
	 *         {@link OtpNode node}.
	 */
	public abstract String[] getNames();

	/**
	 * Determine the {@link OtpErlangPid pid} corresponding to a registered name
	 * on this {@link OtpNode node}.
	 * 
	 * <p>
	 * This method calls a method with the same name in {@link OtpNode#whereis
	 * Otpnode} but is provided here for convenience.
	 * </p>
	 * 
	 * @return the {@link OtpErlangPid pid} corresponding to the registered
	 *         name, or null if the name is not known on this node.
	 */
	public abstract OtpErlangPid whereis(String aname);

	/**
	 * Close this mailbox.
	 * 
	 * <p>
	 * After this operation, the mailbox will no longer be able to receive
	 * messages. Any delivered but as yet unretrieved messages can still be
	 * retrieved however.
	 * </p>
	 * 
	 * <p>
	 * If there are links from this mailbox to other {@link OtpErlangPid pids},
	 * they will be broken when this method is called and exit signals with
	 * reason 'normal' will be sent.
	 * </p>
	 * 
	 * <p>
	 * This is equivalent to {@link #exit(String) exit("normal")}.
	 * </p>
	 */
	public abstract void close();

	/**
	 * Determine if two mailboxes are equal.
	 * 
	 * @return true if both Objects are mailboxes with the same identifying
	 *         {@link OtpErlangPid pids}.
	 */
	public abstract boolean equals(Object o);

	public abstract int hashCode();

	public abstract void breakLinks(OtpErlangObject reason);


	public abstract void setName(String name);

	public abstract void deliver(OtpMsg m);

}