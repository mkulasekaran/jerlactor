/*
 * %CopyrightBegin%
 * 
 * Copyright Ericsson AB 2000-2012. All Rights Reserved.
 * 
 * The contents of this file are subject to the Erlang Public License,
 * Version 1.1, (the "License"); you may not use this file except in
 * compliance with the License. You should have received a copy of the
 * Erlang Public License along with this software. If not, it can be
 * retrieved online at http://www.erlang.org/.
 * 
 * Software distributed under the License is distributed on an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See
 * the License for the specific language governing rights and limitations
 * under the License.
 * 
 * %CopyrightEnd%
 */
package com.ericsson.otp.erlang;

import com.jerlactor.actor.ActorMailBox;
import com.jerlactor.actor.ActorPublisher;

/**
 * <p>
 * Provides a simple mechanism for exchanging messages with Erlang processes or
 * other instances of this class.
 * </p>
 * 
 * <p>
 * Each mailbox is associated with a unique {@link OtpErlangPid pid} that
 * contains information necessary for delivery of messages. When sending
 * messages to named processes or mailboxes, the sender pid is made available to
 * the recipient of the message. When sending messages to other mailboxes, the
 * recipient can only respond if the sender includes the pid as part of the
 * message contents. The sender can determine his own pid by calling
 * {@link #self() self()}.
 * </p>
 * 
 * <p>
 * Mailboxes can be named, either at creation or later. Messages can be sent to
 * named mailboxes and named Erlang processes without knowing the
 * {@link OtpErlangPid pid} that identifies the mailbox. This is neccessary in
 * order to set up initial communication between parts of an application. Each
 * mailbox can have at most one name.
 * </p>
 * 
 * <p>
 * Since this class was intended for communication with Erlang, all of the send
 * methods take {@link OtpErlangObject OtpErlangObject} arguments. However this
 * class can also be used to transmit arbitrary Java objects (as long as they
 * implement one of java.io.Serializable or java.io.Externalizable) by
 * encapsulating the object in a {@link OtpErlangBinary OtpErlangBinary}.
 * </p>
 * 
 * <p>
 * Messages to remote nodes are externalized for transmission, and as a result
 * the recipient receives a <b>copy</b> of the original Java object. To ensure
 * consistent behaviour when messages are sent between local mailboxes, such
 * messages are cloned before delivery.
 * </p>
 * 
 * <p>
 * Additionally, mailboxes can be linked in much the same way as Erlang
 * processes. If a link is active when a mailbox is {@link #close closed}, any
 * linked Erlang processes or OtpMboxes will be sent an exit signal. As well,
 * exit signals will be (eventually) sent if a mailbox goes out of scope and its
 * {@link #finalize finalize()} method called. However due to the nature of
 * finalization (i.e. Java makes no guarantees about when {@link #finalize
 * finalize()} will be called) it is recommended that you always explicitly
 * close mailboxes if you are using links instead of relying on finalization to
 * notify other parties in a timely manner.
 * </p>
 * 
 * <p>
 * When retrieving messages from a mailbox that has received an exit signal, an
 * {@link OtpErlangExit OtpErlangExit} exception will be raised. Note that the
 * exception is queued in the mailbox along with other messages, and will not be
 * raised until it reaches the head of the queue and is about to be retrieved.
 * </p>
 * 
 */
public class OtpMbox implements ActorMailBox {
	OtpNode home;
	OtpErlangPid self;
	GenericQueue queue;
	String name;
	Links links;

	ActorPublisher acp;

	// package constructor: called by OtpNode:createMbox(name)
	// to create a named mbox
	OtpMbox(final OtpNode home, final OtpErlangPid self, final String name) {
		this.self = self;
		this.home = home;
		this.name = name;
		queue = new GenericQueue();
		links = new Links(10);
	}

	// package constructor: called by OtpNode:createMbox()
	// to create an anonymous
	OtpMbox(final OtpNode home, final OtpErlangPid self) {
		this.self = self;
		this.home = home;
		queue = new GenericQueue();
		links = new Links(10);
	}

	OtpMbox(final OtpNode home, final OtpErlangPid self, ActorPublisher acp) {
		this(home, self, null, acp);
	}

	public OtpMbox(final OtpNode home, final OtpErlangPid self,
			final String name, ActorPublisher acp) {
		this.self = self;
		this.home = home;
		this.name = name;
		queue = new GenericQueue();
		links = new Links(10);
		this.acp = acp;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ericsson.otp.erlang.ActorMailBox#self()
	 */
	public OtpErlangPid self() {
		return self;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ericsson.otp.erlang.ActorMailBox#registerName(java.lang.String)
	 */
	public synchronized boolean registerName(final String aname) {
		return home.registerName(aname, this);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ericsson.otp.erlang.ActorMailBox#getName()
	 */
	public String getName() {
		return name;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ericsson.otp.erlang.ActorMailBox#receive()
	 */
	public OtpErlangObject receive() throws OtpErlangExit,
			OtpErlangDecodeException {
		try {
			return receiveMsg().getMsg();
		} catch (final OtpErlangExit e) {
			throw e;
		} catch (final OtpErlangDecodeException f) {
			throw f;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ericsson.otp.erlang.ActorMailBox#receive(long)
	 */
	public OtpErlangObject receive(final long timeout) throws OtpErlangExit,
			OtpErlangDecodeException {
		try {
			final OtpMsg m = receiveMsg(timeout);
			if (m != null) {
				return m.getMsg();
			}
		} catch (final OtpErlangExit e) {
			throw e;
		} catch (final OtpErlangDecodeException f) {
			throw f;
		} catch (final InterruptedException g) {
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ericsson.otp.erlang.ActorMailBox#receiveBuf()
	 */
	public OtpInputStream receiveBuf() throws OtpErlangExit {
		return receiveMsg().getMsgBuf();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ericsson.otp.erlang.ActorMailBox#receiveBuf(long)
	 */
	public OtpInputStream receiveBuf(final long timeout)
			throws InterruptedException, OtpErlangExit {
		final OtpMsg m = receiveMsg(timeout);
		if (m != null) {
			return m.getMsgBuf();
		}

		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ericsson.otp.erlang.ActorMailBox#receiveMsg()
	 */
	public OtpMsg receiveMsg() throws OtpErlangExit {

		final OtpMsg m = (OtpMsg) queue.get();

		switch (m.type()) {
		case OtpMsg.exitTag:
		case OtpMsg.exit2Tag:
			try {
				final OtpErlangObject o = m.getMsg();
				throw new OtpErlangExit(o, m.getSenderPid());
			} catch (final OtpErlangDecodeException e) {
				throw new OtpErlangExit("unknown", m.getSenderPid());
			}

		default:
			return m;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ericsson.otp.erlang.ActorMailBox#receiveMsg(long)
	 */
	public OtpMsg receiveMsg(final long timeout) throws InterruptedException,
			OtpErlangExit {
		final OtpMsg m = (OtpMsg) queue.get(timeout);

		if (m == null) {
			return null;
		}

		switch (m.type()) {
		case OtpMsg.exitTag:
		case OtpMsg.exit2Tag:
			try {
				final OtpErlangObject o = m.getMsg();
				throw new OtpErlangExit(o, m.getSenderPid());
			} catch (final OtpErlangDecodeException e) {
				throw new OtpErlangExit("unknown", m.getSenderPid());
			}

		default:
			return m;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ericsson.otp.erlang.ActorMailBox#send(com.ericsson.otp.erlang.
	 * OtpErlangPid, com.ericsson.otp.erlang.OtpErlangObject)
	 */
	public void send(final OtpErlangPid to, final OtpErlangObject msg) {
		try {
			final String node = to.node();
			if (node.equals(home.node())) {
				home.deliver(new OtpMsg(to, (OtpErlangObject) msg.clone()));
			} else {
				final OtpCookedConnection conn = home.getConnection(node);
				if (conn == null) {
					return;
				}
				conn.send(self, to, msg);
			}
		} catch (final Exception e) {
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ericsson.otp.erlang.ActorMailBox#send(java.lang.String,
	 * com.ericsson.otp.erlang.OtpErlangObject)
	 */
	public void send(final String aname, final OtpErlangObject msg) {
		home.deliver(new OtpMsg(self, aname, (OtpErlangObject) msg.clone()));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ericsson.otp.erlang.ActorMailBox#send(java.lang.String,
	 * java.lang.String, com.ericsson.otp.erlang.OtpErlangObject)
	 */
	public void send(final String aname, final String node,
			final OtpErlangObject msg) {
		try {
			final String currentNode = home.node();
			if (node.equals(currentNode)) {
				send(aname, msg);
			} else if (node.indexOf('@', 0) < 0
					&& node.equals(currentNode.substring(0,
							currentNode.indexOf('@', 0)))) {
				send(aname, msg);
			} else {
				// other node
				final OtpCookedConnection conn = home.getConnection(node);
				if (conn == null) {
					return;
				}
				conn.send(self, aname, msg);
			}
		} catch (final Exception e) {
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ericsson.otp.erlang.ActorMailBox#exit(com.ericsson.otp.erlang.
	 * OtpErlangObject)
	 */
	public void exit(final OtpErlangObject reason) {
		home.closeMbox(this, reason);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ericsson.otp.erlang.ActorMailBox#exit(java.lang.String)
	 */
	public void exit(final String reason) {
		exit(new OtpErlangAtom(reason));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ericsson.otp.erlang.ActorMailBox#exit(com.ericsson.otp.erlang.
	 * OtpErlangPid, com.ericsson.otp.erlang.OtpErlangObject)
	 */
	// it's called exit, but it sends exit2
	public void exit(final OtpErlangPid to, final OtpErlangObject reason) {
		exit(2, to, reason);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ericsson.otp.erlang.ActorMailBox#exit(com.ericsson.otp.erlang.
	 * OtpErlangPid, java.lang.String)
	 */
	public void exit(final OtpErlangPid to, final String reason) {
		exit(to, new OtpErlangAtom(reason));
	}

	// this function used internally when "process" dies
	// since Erlang discerns between exit and exit/2.
	private void exit(final int arity, final OtpErlangPid to,
			final OtpErlangObject reason) {
		try {
			final String node = to.node();
			if (node.equals(home.node())) {
				home.deliver(new OtpMsg(OtpMsg.exitTag, self, to, reason));
			} else {
				final OtpCookedConnection conn = home.getConnection(node);
				if (conn == null) {
					return;
				}
				switch (arity) {
				case 1:
					conn.exit(self, to, reason);
					break;

				case 2:
					conn.exit2(self, to, reason);
					break;
				}
			}
		} catch (final Exception e) {
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ericsson.otp.erlang.ActorMailBox#link(com.ericsson.otp.erlang.
	 * OtpErlangPid)
	 */
	public void link(final OtpErlangPid to) throws OtpErlangExit {
		try {
			final String node = to.node();
			if (node.equals(home.node())) {
				if (!home.deliver(new OtpMsg(OtpMsg.linkTag, self, to))) {
					throw new OtpErlangExit("noproc", to);
				}
			} else {
				final OtpCookedConnection conn = home.getConnection(node);
				if (conn != null) {
					conn.link(self, to);
				} else {
					throw new OtpErlangExit("noproc", to);
				}
			}
		} catch (final OtpErlangExit e) {
			throw e;
		} catch (final Exception e) {
		}

		links.addLink(self, to);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ericsson.otp.erlang.ActorMailBox#unlink(com.ericsson.otp.erlang.
	 * OtpErlangPid)
	 */
	public void unlink(final OtpErlangPid to) {
		links.removeLink(self, to);

		try {
			final String node = to.node();
			if (node.equals(home.node())) {
				home.deliver(new OtpMsg(OtpMsg.unlinkTag, self, to));
			} else {
				final OtpCookedConnection conn = home.getConnection(node);
				if (conn != null) {
					conn.unlink(self, to);
				}
			}
		} catch (final Exception e) {
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ericsson.otp.erlang.ActorMailBox#ping(java.lang.String, long)
	 */
	public boolean ping(final String node, final long timeout) {
		return home.ping(node, timeout);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ericsson.otp.erlang.ActorMailBox#getNames()
	 */
	public String[] getNames() {
		return home.getNames();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ericsson.otp.erlang.ActorMailBox#whereis(java.lang.String)
	 */
	public OtpErlangPid whereis(final String aname) {
		return home.whereis(aname);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ericsson.otp.erlang.ActorMailBox#close()
	 */
	public void close() {
		home.closeMbox(this);
	}

	@Override
	protected void finalize() {
		close();
		queue.flush();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ericsson.otp.erlang.ActorMailBox#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(final Object o) {
		if (!(o instanceof OtpMbox)) {
			return false;
		}

		final OtpMbox m = (OtpMbox) o;
		return m.self.equals(self);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ericsson.otp.erlang.ActorMailBox#hashCode()
	 */
	@Override
	public int hashCode() {
		return self.hashCode();
	}

	/*
	 * called by OtpNode to deliver message to this mailbox.
	 * 
	 * About exit and exit2: both cause exception to be raised upon receive().
	 * However exit (not 2) causes any link to be removed as well, while exit2
	 * leaves any links intact.
	 */
	// TODO:Task here is to set the notify flag
	public void deliver(OtpMsg m) {
		switch (m.type()) {
		case OtpMsg.linkTag:
			links.addLink(self, m.getSenderPid());
			break;

		case OtpMsg.unlinkTag:
			links.removeLink(self, m.getSenderPid());
			break;

		case OtpMsg.exitTag:
			links.removeLink(self, m.getSenderPid());

			sendMessage(m);
			// queue.put(m);

			break;

		case OtpMsg.exit2Tag:
		default:
			sendMessage(m);
			// queue.put(m);
			break;
		}
	}

	public void sendMessage(OtpMsg m) {
		if (acp != null) {
			acp.deliver(m);
		} else {
			queue.put(m);
		}
	}

	// used to break all known links to this mbox
	public void breakLinks(final OtpErlangObject reason) {
		final Link[] l = links.clearLinks();

		if (l != null) {
			final int len = l.length;

			for (int i = 0; i < len; i++) {
				exit(1, l[i].remote(), reason);
			}
		}
	}

	public void setName(String name) {
		this.name = name;
	}



}
